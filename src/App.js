
import React ,{useState,useEffect} from 'react';
import './App.css';

function App() {

  const [todos, setTodos] = useState([]);
  const [todo, setTodo] = useState("");
  const [todoEditing, setTodoEditing] = useState(null);
  const [editingText, setEditingText] = useState("");

  useEffect(() => {
    const json = localStorage.getItem("todos");
    const loadedTodos = JSON.parse(json);
    if (loadedTodos) {
      setTodos(loadedTodos);
    }
  }, []);

  useEffect(() => {
    const json = JSON.stringify(todos);
    localStorage.setItem("todos", json);
  }, [todos])

  const handleSubmit = (e) => {
    e.preventDefault();

    const newTodo = {
      id: new Date().getTime(),
      text: todo,
    };
    setTodos([...todos].concat(newTodo));
    setTodo("");
  }

  const handledelete = (id) =>{
    const udatedtodos = [...todos].filter( (filterTodo) => filterTodo.id !== id);
    setTodos(udatedtodos)
  }

  const edittodo = (id) => {
   const updatedTodos = [...todos].map((todo) => {
     if (todo.id === id){
      todo.text = editingText
     }
     return todo
   });
   setTodos(updatedTodos);
   setTodoEditing(null);
  }
  
  return (
    <div className="App">

      <h1>Todo List</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          onChange={(e) => setTodo(e.target.value)}
          value={todo}
        />
        <button type="submit">Add Todo</button>
      </form>
      {todos.map((item) => 
      <div key={item.id}>


{todoEditing == item.id ? (<input type="text" onChange={(e) => setEditingText(e.target.value)} value={editingText}></input>) : (<h1>{item.text}</h1>)}

    
{ todoEditing  === item.id ? (<button type="submit" onClick={() => edittodo(item.id)}>submit edit</button> )  :(<button type="submit" onClick={() => setTodoEditing(item.id)}>edit</button> )}
     
      
      <button type="submit" onClick={() => handledelete(item.id)}>delete</button>
      </div>) }
    </div>
  );

}

export default App;
